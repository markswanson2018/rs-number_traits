rs-number_traits
=====

number traits

```rust
extern crate number_traits;


use number_traits::{Num, Sqrt};


fn add<T>(a: T, b: T) -> T
    where T: Num,
{
    a + b
}

fn sqrt<T>(x: T) -> <T as Sqrt>::Output
    where T: Sqrt,
{
    x.sqrt()
}


fn main() {
    assert_eq!(add(2, 2), 4);
    assert_eq!(add(2.0, 2.0), 4);
    assert_eq!(sqrt(4), 2);
    assert_eq!(sqrt(4.0), 2);
}
```
